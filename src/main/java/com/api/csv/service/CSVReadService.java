package com.api.csv.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;

@Service
public class CSVReadService {

	private static String UPLOADED_FOLDER = "/home/buddy4study/Desktop/facetRead.csv";
	private static String accessToken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJyZWFkIiwid3JpdGUiLCJ0cnVzdCJdLCJleHAiOjE2MTQ0NTU3MTEsImF1dGhvcml0aWVzIjpbIlVTRVIiXSwianRpIjoiY2JlZDBkNTctYzdlMi00MWMxLWI4MzEtN2FhYjFlMjMyNzE0IiwiY2xpZW50X2lkIjoiYjRzIn0.tzfMWk2Z5Rv3WbC2ljL93a-LIpG0nqqxzN6sAw5HgZU";
	private static String url="https://api.buddy4study.com/api/v1.0/ssms/scholarship/iit-kharagpur-department-of-mechanical-engineering-senior-research-fellowship-2020/facetlookup?mode=OPEN";
	 @Autowired
	 ObjectMapper objectMapper;
	 
	public void readfile1() {
	    RestTemplate restTemplate=new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));        
	    headers.add("User-Agent", "Spring's RestTemplate" );  // value can be whatever
	    headers.add("Authorization", "Bearer "+accessToken );

	    ResponseEntity<String> response = restTemplate.exchange(url,
	            HttpMethod.GET,
	            new HttpEntity<String>("parameters", headers),
	            String.class
	    );
	    System.out.println(response.getBody());
	}

	public void readfile() {
		RestTemplate restTemplate = new RestTemplate();
		try {
			FileInputStream fileInputStream = new FileInputStream(UPLOADED_FOLDER);
			BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));

			File file = new File("/home/buddy4study/Documents/facetWrite.csv");
			FileWriter output = new FileWriter(file);
			CSVWriter write = new CSVWriter(output);
			// Header column value
			String[] header = { "Slug", "Mode", "Count", "Scholarships Source", "Filter" };
			write.writeNext(header);

			String line = "";
			while ((line = br.readLine()) != null) {
				// System.out.println("Coulmns = " + line);
				String cvsSplitBy = ",";
				String facetUrl = null;
				String[] cols = line.split(cvsSplitBy);
				for (int i = 0; i < cols.length; i++) {
					facetUrl = "https://api.buddy4study.com/api/v1.0/ssms/scholarship/" + cols[0]
							+ "/facetlookup?mode=OPEN";
					System.out.println("Coulmns = " + facetUrl);
					HttpHeaders headers = createHttpHeaders();
					ResponseEntity<String> response = restTemplate.exchange(facetUrl, HttpMethod.GET,
							new HttpEntity<String>("parameters", headers), String.class);
					System.out.println(cols[i]);
					String respoFeacet = response.getBody();
					try {
						String totalCount="";
						JSONParser parser = new JSONParser();
						JSONObject pagesObject = (JSONObject) parser.parse(respoFeacet);
						String filters = pagesObject.get("filters").toString();
						String scholarshipsSource = pagesObject.get("scholarshipsSource").toString();
						pagesObject = (JSONObject) parser.parse(pagesObject.get("scholarships").toString());
						for (Iterator iterator = pagesObject.keySet().iterator(); iterator.hasNext();) {
							String key = (String) iterator.next();
							totalCount=pagesObject.get("total").toString();
						}
						System.out.println("Slug: "+cols[i]+" Total Count: "+totalCount);
						String[] data1 = { cols[i], "OPEN", totalCount, scholarshipsSource,
								filters };
						write.writeNext(data1);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				// System.exit(0);
			}
			br.close();
			write.close();
			System.out.println("Done..");
			// getTemplate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	private void fileWirte() {
		File files = new File("/home/buddy4study/Documents/test.csv");
        try {
            FileWriter output = new FileWriter(files);
            CSVWriter write = new CSVWriter(output);

            // Header column value
            String[] header = { "ID", "Name", "Address", "Phone Number" };
            write.writeNext(header);
            // Value
            String[] data11 = { "1", "First Name", "Address1", "12345" };
            write.writeNext(data11);
            String[] data2 = { "2", "Second Name", "Address2", "123456" };
            write.writeNext(data2);
            String[] data3 = { "3", "Third Name", "Address3", "1234567" };
            write.writeNext(data3);
            write.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }

	}
	private HttpHeaders createHttpHeaders()
	{
		HttpHeaders headers = new HttpHeaders();
		 headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));        
		    headers.add("User-Agent", "Spring's RestTemplate" );  // value can be whatever
		    headers.add("Authorization", "Bearer "+accessToken );
		return headers;
	}
}
