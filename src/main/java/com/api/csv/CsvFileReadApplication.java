package com.api.csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsvFileReadApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsvFileReadApplication.class, args);
	}

}
