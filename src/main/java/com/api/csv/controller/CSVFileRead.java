package com.api.csv.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.csv.service.CSVReadService;

@RestController
public class CSVFileRead {

	private CSVReadService cSVReadService;
	public CSVFileRead(CSVReadService cSVReadService) {
		super();
		this.cSVReadService = cSVReadService;
	}


	@GetMapping("/upload") // //new annotation since 4.3
	public void singleFileUpload() {

        cSVReadService.readfile();
        
    }
}
